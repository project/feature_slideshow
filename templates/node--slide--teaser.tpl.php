<?php print render($content['field_slide_image']); ?>
<div class='slide-content-wrapper <?php print $slide_style; ?>'>
  <div class="slide-title"><?php print $slide_title; ?></div>
  <div class="slide-subtitle hidden-xs"><?php print $slide_subtitle; ?></div>
  <?php  print $slide_button; ?>
</div>

